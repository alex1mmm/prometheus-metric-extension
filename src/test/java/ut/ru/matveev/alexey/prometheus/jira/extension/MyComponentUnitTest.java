package ut.ru.matveev.alexey.prometheus.jira.extension;

import org.junit.Test;
import ru.matveev.alexey.prometheus.jira.extension.api.MyPluginComponent;
import ru.matveev.alexey.prometheus.jira.extension.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}