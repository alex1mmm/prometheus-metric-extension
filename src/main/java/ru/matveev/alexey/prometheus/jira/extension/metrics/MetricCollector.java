package ru.matveev.alexey.prometheus.jira.extension.metrics;

import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetricCollector;

public class MetricCollector extends PrometheusMetricCollector {

    public MetricCollector(VipIssuesNotClosed vipIssuesNotClosed,
                           IssueCountInDatabaseIndexSummary issueCountInDatabaseIndexSummary) {
        this.getMetrics().add(vipIssuesNotClosed);
        this.getMetrics().add(issueCountInDatabaseIndexSummary);
    }
}
