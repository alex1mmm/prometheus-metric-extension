package ru.matveev.alexey.prometheus.jira.extension.metrics.indexsummary;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@XmlRootElement
public class IssueIndexSummaryBean {

    public static final IssueIndexSummaryBean DOC_EXAMPLE;

    static {
        DOC_EXAMPLE = new IssueIndexSummaryBean();
        DOC_EXAMPLE.indexReadable = true;
        DOC_EXAMPLE.countInDatabase = 12072L;
        DOC_EXAMPLE.countInIndex = 10072L;
        DOC_EXAMPLE.countInArchive = 2000L;
        DOC_EXAMPLE.lastUpdatedInDatabase = ZonedDateTime.of(2017, 7, 8, 1, 46, 16, 940, ZoneId.of("+1")).toInstant();
        DOC_EXAMPLE.lastUpdatedInIndex = ZonedDateTime.of(2017, 7, 8, 0, 48, 53, 0, ZoneId.of("+1")).toInstant();
    }

    @XmlElement
    private Boolean indexReadable;

    @XmlElement
    private Long countInDatabase;

    @XmlElement
    private Long countInIndex;

    @XmlElement
    private Long countInArchive;

    private Instant lastUpdatedInDatabase;

    private Instant lastUpdatedInIndex;

    public Boolean getIndexReadable() {
        return indexReadable;
    }

    public void setIndexReadable(final Boolean indexReadable) {
        this.indexReadable = indexReadable;
    }

    public Long getCountInDatabase() {
        return countInDatabase;
    }

    public void setCountInDatabase(final Long countInDatabase) {
        this.countInDatabase = countInDatabase;
    }

    public Long getCountInIndex() {
        return countInIndex;
    }

    public void setCountInIndex(final Long countInIndex) {
        this.countInIndex = countInIndex;
    }

    public Long getCountInArchive() {
        return countInArchive;
    }

    public void setCountInArchive(final Long countInArchive) {
        this.countInArchive = countInArchive;
    }

    public Instant getLastUpdatedInDatabase() {
        return lastUpdatedInDatabase;
    }

    public void setLastUpdatedInDatabase(final Instant lastUpdatedInDatabase) {
        this.lastUpdatedInDatabase = lastUpdatedInDatabase;
    }

    public Instant getLastUpdatedInIndex() {
        return lastUpdatedInIndex;
    }

    public void setLastUpdatedInIndex(final Instant lastUpdatedInIndex) {
        this.lastUpdatedInIndex = lastUpdatedInIndex;
    }
}
