package ru.matveev.alexey.prometheus.jira.extension.metrics;

import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetric;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.bean.PagerFilter;
import io.prometheus.client.Gauge;
import org.apache.commons.collections.map.HashedMap;

import javax.inject.Named;
import java.util.Map;

@Named
public class VipIssuesNotClosed extends PrometheusMetric {

    private final Gauge vipIssuesNotClosedMetric = Gauge.build()
            .name("vip_issues_not_closed")
            .help("Shows the number of issues which are in the VIP project and not clsed withing 2 days. Label with the assignee of the issue is available.")
            .labelNames("assignee")
            .create();

    public VipIssuesNotClosed() {
        super("vip_issues_not_closed", "Shows the number of issues which are in the VIP project and not clsed withing 2 days. Label with the assignee of the issue is available.", true, true);
    }

    @Override
    public PrometheusMetricResult collect() {
        Map<String, Integer> assigneeMap = new HashedMap();
        UserManager userManager = ComponentAccessor.getUserManager();
        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        ApplicationUser appUser = userManager.getUserByKey("admin");
        String jqlSearch = "project = VIP and created < -2d and resolution is empty";
        SearchService.ParseResult parseResult =  searchService.parseQuery(appUser, jqlSearch);
        if (parseResult.isValid()) {
            SearchResults<Issue> searchResult = null;
            try {
                searchResult = searchService.search(appUser, parseResult.getQuery(), PagerFilter.getUnlimitedFilter());
                for (Issue issue : searchResult.getResults()) {
                   String assignee = issue.getAssignee().getUsername();
                   if (assigneeMap.containsKey(assignee)) {
                       assigneeMap.put(assignee, assigneeMap.get(assignee) + 1);
                   } else {
                       assigneeMap.put(assignee, 1);
                   }
                }
                for (Map.Entry<String, Integer> entry : assigneeMap.entrySet()) {
                    this.vipIssuesNotClosedMetric.labels(entry.getKey()).set(entry.getValue());
                }
            } catch (SearchException e) {
                e.printStackTrace();
            }

        }

    return new PrometheusMetricResult(this.vipIssuesNotClosedMetric.collect());
    }
}
