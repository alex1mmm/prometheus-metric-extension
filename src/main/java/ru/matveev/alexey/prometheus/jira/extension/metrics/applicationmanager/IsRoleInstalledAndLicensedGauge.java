package ru.matveev.alexey.prometheus.jira.extension.metrics.applicationmanager;

import com.anovaapps.atlassian.prometheus.spi.api.PrecalculatedValue;
import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetric;
import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import io.prometheus.client.Gauge;

import javax.inject.Named;
import java.util.List;

@Named
public class IsRoleInstalledAndLicensedGauge extends PrometheusMetric {

    private final Gauge isRoleInstalledAndLicensedMetric = Gauge.build()
            .name("jira_is_role_installed_and_licensed")
            .help("if the role installed and licensed")
            .labelNames("licenseType")
            .create();

    public IsRoleInstalledAndLicensedGauge(@ComponentImport ApplicationManager applicationManager,
                                           @ComponentImport ApplicationRoleManager applicationRoleManager) {
        super("jira_is_role_installed_and_licensed", "if the role installed and licensed", true, true);
    }

    @Override
    public PrometheusMetricResult collect(List<PrecalculatedValue> precalculatedValues) {
        PrecalculatedValue value = this.getPrecalculatedValuesForMetric(precalculatedValues, "jira_is_role_installed_and_licensed").get(0);
        this.isRoleInstalledAndLicensedMetric.labels( value.getLabels().get(0)).set(value.getValue());
        return new PrometheusMetricResult(this.isRoleInstalledAndLicensedMetric.collect());
    }

}
