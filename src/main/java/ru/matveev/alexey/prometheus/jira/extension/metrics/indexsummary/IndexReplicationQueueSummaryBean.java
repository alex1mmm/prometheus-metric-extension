package ru.matveev.alexey.prometheus.jira.extension.metrics.indexsummary;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IndexReplicationQueueSummaryBean {

    public static final IndexReplicationQueueSummaryBean DOC_EXAMPLE_1;
    public static final IndexReplicationQueueSummaryBean DOC_EXAMPLE_2;

    static {
        DOC_EXAMPLE_1 = new IndexReplicationQueueSummaryBean();
        DOC_EXAMPLE_1.lastConsumedOperation = IndexReplicationQueueEntryBean.DOC_EXAMPLE_1;
        DOC_EXAMPLE_1.lastOperationInQueue = IndexReplicationQueueEntryBean.DOC_EXAMPLE_1;
        DOC_EXAMPLE_1.queueSize = 0L;
        DOC_EXAMPLE_2 = new IndexReplicationQueueSummaryBean();
        DOC_EXAMPLE_2.lastConsumedOperation = IndexReplicationQueueEntryBean.DOC_EXAMPLE_2;
        DOC_EXAMPLE_2.lastOperationInQueue = IndexReplicationQueueEntryBean.DOC_EXAMPLE_2;
        DOC_EXAMPLE_2.queueSize = 0L;
    }

    @XmlElement
    private IndexReplicationQueueEntryBean lastConsumedOperation;

    @XmlElement
    private IndexReplicationQueueEntryBean lastOperationInQueue;

    @XmlElement
    private Long queueSize;

    public IndexReplicationQueueEntryBean getLastConsumedOperation() {
        return lastConsumedOperation;
    }

    public void setLastConsumedOperation(final IndexReplicationQueueEntryBean lastConsumedOperation) {
        this.lastConsumedOperation = lastConsumedOperation;
    }

    public IndexReplicationQueueEntryBean getLastOperationInQueue() {
        return lastOperationInQueue;
    }

    public void setLastOperationInQueue(final IndexReplicationQueueEntryBean lastOperationInQueue) {
        this.lastOperationInQueue = lastOperationInQueue;
    }

    public Long getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(final Long queueSize) {
        this.queueSize = queueSize;
    }
}
