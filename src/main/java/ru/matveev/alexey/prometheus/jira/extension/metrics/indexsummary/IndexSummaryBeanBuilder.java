package ru.matveev.alexey.prometheus.jira.extension.metrics.indexsummary;

import com.atlassian.jira.index.summary.IndexReplicationQueueSummary;
import com.atlassian.jira.index.summary.IssueIndexSummary;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Builder for {@link IndexSummaryBean} object.
 * One instance of this builder class can build only single product object.
 *
 * @see IndexSummaryBean
 * @see IssueIndexSummaryBean
 * @see IndexReplicationQueueSummaryBean
 * @see IndexReplicationQueueEntryBean
 */
public class IndexSummaryBeanBuilder {

    private final IndexSummaryBean indexSummaryBean = new IndexSummaryBean();

    public IndexSummaryBeanBuilder nodeId(final String nodeId) {
        indexSummaryBean.setNodeId(nodeId);
        return this;
    }

    public IndexSummaryBeanBuilder reportTime(final Instant time) {
        indexSummaryBean.setReportTime(time);
        return this;
    }

    public IndexSummaryBeanBuilder queue(final String sourceNodeId, final IndexReplicationQueueSummary queue) {
        final IndexReplicationQueueSummaryBean queueBean = lazyGetReplicationQueue(sourceNodeId);
        queue.getLastOperationInQueue().ifPresent((lastOperationInQueue) ->
                queueBean.setLastOperationInQueue(new IndexReplicationQueueEntryBean(
                        lastOperationInQueue.getId(),
                        lastOperationInQueue.getReplicationTime()
                )
        ));
        queue.getLastConsumedOperation().ifPresent((lastConsumedOperation) ->
                queueBean.setLastConsumedOperation(new IndexReplicationQueueEntryBean(
                        lastConsumedOperation.getId(),
                        lastConsumedOperation.getReplicationTime()
                )
        ));
        queueBean.setQueueSize(queue.getQueueSize());
        return this;
    }

    public IndexSummaryBeanBuilder issueIndex(final IssueIndexSummary issueIndexSummary) {
        final IssueIndexSummaryBean issueIndexSummaryBean = lazyGetIssueIndex();
        if (issueIndexSummary.isIndexReadable()) {
            issueIndexSummaryBean.setIndexReadable(true);
            issueIndexSummaryBean.setCountInDatabase(issueIndexSummary.getCountInDatabase());
            issueIndexSummaryBean.setCountInIndex(issueIndexSummary.getCountInIndex());
            issueIndexSummaryBean.setCountInArchive(issueIndexSummary.getCountInArchive());
            issueIndexSummaryBean.setLastUpdatedInDatabase(issueIndexSummary.getLastUpdatedInDatabase());
            issueIndexSummaryBean.setLastUpdatedInIndex(issueIndexSummary.getLastUpdatedInIndex());
        } else {
            issueIndexSummaryBean.setIndexReadable(false);
        }
        return this;
    }

    private IssueIndexSummaryBean lazyGetIssueIndex() {
        IssueIndexSummaryBean issueIndexSummaryBean = indexSummaryBean.getIssueIndex();
        if (issueIndexSummaryBean == null) {
            issueIndexSummaryBean = new IssueIndexSummaryBean();
            indexSummaryBean.setIssueIndex(issueIndexSummaryBean);
        }
        return issueIndexSummaryBean;
    }

    private IndexReplicationQueueSummaryBean lazyGetReplicationQueue(final String nodeId) {
        final Map<String, IndexReplicationQueueSummaryBean> replicationQueues = lazyGetReplicationQueues();
        return replicationQueues.computeIfAbsent(nodeId, k -> new IndexReplicationQueueSummaryBean());
    }

    private Map<String, IndexReplicationQueueSummaryBean> lazyGetReplicationQueues() {
        Map<String, IndexReplicationQueueSummaryBean> replicationQueues = indexSummaryBean.getReplicationQueues();
        if (replicationQueues == null) {
            replicationQueues = new HashMap<>();
            indexSummaryBean.setReplicationQueues(replicationQueues);
        }
        return replicationQueues;
    }

    public IndexSummaryBean build() {
        return indexSummaryBean;
    }
}
