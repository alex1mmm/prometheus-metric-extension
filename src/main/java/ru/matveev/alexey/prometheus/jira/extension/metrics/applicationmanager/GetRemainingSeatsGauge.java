package ru.matveev.alexey.prometheus.jira.extension.metrics.applicationmanager;

import com.anovaapps.atlassian.prometheus.spi.api.PrecalculatedValue;
import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetric;
import io.prometheus.client.Gauge;

import javax.inject.Named;
import java.util.List;

@Named
public class GetRemainingSeatsGauge extends PrometheusMetric {

    private final Gauge jira_get_remaining_seats = Gauge.build()
            .name("jira_get_remaining_seats")
            .help("How many remaining seats left")
            .labelNames("licenseType")
            .create();

    public GetRemainingSeatsGauge() {
        super("jira_get_remaining_seats", "How many remaining seats left", true, true);
    }

    @Override
    public PrometheusMetricResult collect(List<PrecalculatedValue> precalculatedValues) {
        PrecalculatedValue value = this.getPrecalculatedValuesForMetric(precalculatedValues, "jira_get_remaining_seats").get(0);
        this.jira_get_remaining_seats.labels( value.getLabels().get(0)).set(value.getValue());
        return new PrometheusMetricResult(this.jira_get_remaining_seats.collect());
    }

}

