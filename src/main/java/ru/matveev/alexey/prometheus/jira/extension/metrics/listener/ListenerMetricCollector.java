package ru.matveev.alexey.prometheus.jira.extension.metrics.listener;

import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetricCollector;

public class ListenerMetricCollector extends PrometheusMetricCollector {

    public ListenerMetricCollector(WatcherCounter watcherCounter) {
        this.getMetrics().add(watcherCounter);
    }
}
