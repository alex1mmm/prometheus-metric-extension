package ru.matveev.alexey.prometheus.jira.extension.api;

public interface MyPluginComponent
{
    String getName();
}