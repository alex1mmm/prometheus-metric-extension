package ru.matveev.alexey.prometheus.jira.extension.metrics.indexsummary;



import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@XmlRootElement
public class IndexReplicationQueueEntryBean {

    public static final IndexReplicationQueueEntryBean DOC_EXAMPLE_1;
    public static final IndexReplicationQueueEntryBean DOC_EXAMPLE_2;

    static {
        DOC_EXAMPLE_1 = new IndexReplicationQueueEntryBean();
        DOC_EXAMPLE_1.id = 16822L;
        DOC_EXAMPLE_1.replicationTime = ZonedDateTime.of(2017, 7, 8, 0, 49, 7, 842, ZoneId.of("+1")).toInstant();
        DOC_EXAMPLE_2 = new IndexReplicationQueueEntryBean();
        DOC_EXAMPLE_2.id = 16522L;
        DOC_EXAMPLE_1.replicationTime = ZonedDateTime.of(2017, 7, 8, 0, 10, 56, 169, ZoneId.of("+1")).toInstant();
    }

    @XmlElement
    private Long id;


    private Instant replicationTime;

    public IndexReplicationQueueEntryBean() {
    }

    public IndexReplicationQueueEntryBean(final Long id, final Instant replicationTime) {
        this.id = id;
        this.replicationTime = replicationTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Instant getReplicationTime() {
        return replicationTime;
    }

    public void setReplicationTime(final Instant replicationTime) {
        this.replicationTime = replicationTime;
    }
}
