package ru.matveev.alexey.prometheus.jira.extension.metrics.indexsummary;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
public class IndexSummaryBean {



    @XmlElement
    private String nodeId;

    private Instant reportTime;

    @XmlElement
    private IssueIndexSummaryBean issueIndex;

    @XmlElement
    private Map<String, IndexReplicationQueueSummaryBean> replicationQueues;

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(final String nodeId) {
        this.nodeId = nodeId;
    }

    public Instant getReportTime() {
        return reportTime;
    }

    public void setReportTime(final Instant reportTime) {
        this.reportTime = reportTime;
    }

    public IssueIndexSummaryBean getIssueIndex() {
        return issueIndex;
    }

    public void setIssueIndex(final IssueIndexSummaryBean issueIndex) {
        this.issueIndex = issueIndex;
    }

    public Map<String, IndexReplicationQueueSummaryBean> getReplicationQueues() {
        return replicationQueues;
    }

    public void setReplicationQueues(final Map<String, IndexReplicationQueueSummaryBean> replicationQueues) {
        this.replicationQueues = replicationQueues;
    }
}
