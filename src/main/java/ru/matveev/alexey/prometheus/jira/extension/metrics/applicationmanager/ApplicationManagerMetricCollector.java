package ru.matveev.alexey.prometheus.jira.extension.metrics.applicationmanager;

import com.anovaapps.atlassian.prometheus.spi.api.PrecalculatedValue;
import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetricCollector;
import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ApplicationManagerMetricCollector extends PrometheusMetricCollector {
    private final ApplicationManager applicationManager;
    private final ApplicationRoleManager applicationRoleManager;

    public ApplicationManagerMetricCollector(@ComponentImport ApplicationManager applicationManager,
                                             @ComponentImport ApplicationRoleManager applicationRoleManager,
                                             GetRemainingSeatsGauge getRemainingSeatsGauge,
                                             IsRoleInstalledAndLicensedGauge isRoleInstalledAndLicensedGauge) {
        this.getMetrics().add(getRemainingSeatsGauge);
        this.getMetrics().add(isRoleInstalledAndLicensedGauge);
        this.applicationManager = applicationManager;
        this.applicationRoleManager = applicationRoleManager;
    }

    @Override
    protected List<PrecalculatedValue> getPrecalculatedValues() {
        List<PrecalculatedValue> result = new ArrayList<>();
        for (Application application : this.applicationManager.getApplications()) {
            if (application != null) {
                SingleProductLicenseDetailsView singleProductLicenseDetailsView = application.getLicense().getOrNull();
                if (singleProductLicenseDetailsView != null) {
                    double remainingSeatsValue = applicationRoleManager.getRemainingSeats(application.getKey());
                    double isRoleInstalledAndLicensedValue = getBoolean(applicationRoleManager.isRoleInstalledAndLicensed(application.getKey()));

                    result.add(new PrecalculatedValue("jira_get_remaining_seats", Arrays.asList(application.getName()), remainingSeatsValue));
                    result.add(new PrecalculatedValue("jira_is_role_installed_and_licensed", Arrays.asList(application.getName()), isRoleInstalledAndLicensedValue));

                }
            }
        }
        return result;
    }
}
