package ru.matveev.alexey.prometheus.jira.extension.metrics;

import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetric;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.index.summary.DefaultIndexSummarizer;
import com.atlassian.jira.index.summary.IndexReplicationQueueSummary;
import com.atlassian.jira.index.summary.IndexSummarizer;
import com.atlassian.jira.index.summary.IssueIndexSummary;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import io.prometheus.client.Gauge;
import ru.matveev.alexey.prometheus.jira.extension.metrics.indexsummary.IndexSummaryBean;
import ru.matveev.alexey.prometheus.jira.extension.metrics.indexsummary.IndexSummaryBeanBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.Instant;
import java.util.Map;

@Named
public class IssueCountInDatabaseIndexSummary extends PrometheusMetric {
    private final ClusterManager clusterManager;
    private final IndexSummarizer indexSummarizer;

    private final Gauge issueCountInDatabaseIndexSummaryMetric = Gauge.build()
            .name("issue_count_in_db_index_summary")
            .help("number of issue in db provide by IndexSummary class")
            .labelNames("nodeid")
            .create();

    @Inject
    public IssueCountInDatabaseIndexSummary(@ComponentImport final ClusterManager clusterManager,
                                            @ComponentImport final IndexSummarizer indexSummarizer) {
        super("issue_count_in_db_index_summary", "number of issue in db provide by IndexSummary class", true, true);
        this.clusterManager = clusterManager;
        this.indexSummarizer = indexSummarizer;
    }

    @Override
    public PrometheusMetricResult collect() {
        final IndexSummaryBeanBuilder beanBuilder = new IndexSummaryBeanBuilder();
        final DefaultIndexSummarizer defaultIndexSummarizer = ComponentAccessor.getComponent(DefaultIndexSummarizer.class);
        final ServiceOutcome<IssueIndexSummary> issueIndexSummaryOutcome = defaultIndexSummarizer.summarizeIssueIndex();
        IssueIndexSummary issueIndexSummary = issueIndexSummaryOutcome.get();
        beanBuilder.issueIndex(issueIndexSummary);

        if (clusterManager.isClustered()) {
            final ServiceOutcome<Map<String, IndexReplicationQueueSummary>> queueSummaryOutcome =
                    defaultIndexSummarizer.summarizeIndexReplicationQueues();

            queueSummaryOutcome.get().forEach(beanBuilder::queue);
            beanBuilder.nodeId(clusterManager.getNodeId());
        }

        beanBuilder.reportTime(Instant.now());
        IndexSummaryBean result = beanBuilder.build();
        String nodeId = result.getNodeId() != null ? result.getNodeId() : "not_clustered";
        this.issueCountInDatabaseIndexSummaryMetric.labels(nodeId).set(result.getIssueIndex().getCountInDatabase());

        return new PrometheusMetricResult(this.issueCountInDatabaseIndexSummaryMetric.collect());
    }
}
