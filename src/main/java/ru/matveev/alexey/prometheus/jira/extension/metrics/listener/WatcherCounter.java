package ru.matveev.alexey.prometheus.jira.extension.metrics.listener;

import com.anovaapps.atlassian.prometheus.spi.api.util.AbstractListenerMetric;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import io.prometheus.client.Counter;
import com.atlassian.jira.event.issue.IssueWatcherAddedEvent;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class WatcherCounter extends AbstractListenerMetric {

    private Counter watcherMetric =  Counter.build()
            .name("user_watched")
            .help("shows how many times a user has been set as a watcher to an issue")
            .labelNames("watcher")
            .create();


    @Inject
    protected WatcherCounter(@ComponentImport EventPublisher eventPublisher) {
        super(eventPublisher, "user_watched", "shows how many times a user has been set as a watcher to an issue", true, true);
    }

    @EventListener
    public void onUserWatched(IssueWatcherAddedEvent event) {
        this.watcherMetric.labels(event.getApplicationUser().getKey()).inc();
    }

    @Override
    public PrometheusMetricResult collect() {
        return new PrometheusMetricResult(this.watcherMetric.collect());
    }
}
