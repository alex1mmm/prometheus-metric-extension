package ru.matveev.alexey.prometheus.jira.extension.metrics.requestmetric;

import com.anovaapps.atlassian.prometheus.spi.api.PrometheusRequestMetric;
import io.prometheus.client.Histogram;

import javax.inject.Named;
import javax.servlet.ServletRequest;

@Named
public class RequestHistogram extends PrometheusRequestMetric {
    Histogram simpleRequestetric = Histogram.build()
            .buckets(1, 5, 10, 20)
            .name("simple_request_metric")
            .help("Request duration on path")
            .labelNames("path")
            .create();

    public RequestHistogram() {
        super("simple_request_metric", "Request duration on path", true, true);
    }


    @Override
    public Histogram.Timer startTimer(String path, String queryString, ServletRequest servletRequest) {
        return this.simpleRequestetric.labels(path).startTimer();
    }

    @Override
    public PrometheusMetricResult collect() {
        return new PrometheusMetricResult(simpleRequestetric.collect());
    }

}

